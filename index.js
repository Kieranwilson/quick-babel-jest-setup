export default (something) => new Promise(resolve => setInterval(() => {
    const squared = something * something
    resolve(squared)
}, 1000))
