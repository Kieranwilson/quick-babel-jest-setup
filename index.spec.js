import something from './index'

describe('something', () => {
    it('works via babel', async () => {
        const result = await (something(5))
        expect(result).toEqual(25)
    })
})
